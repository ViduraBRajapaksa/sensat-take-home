#### Basic Task Implemented

Write an Angular application which

1.  reads the records from the sensor_readings.json
2.  displays them in a tabular component (nothing too ugly)
3.  allows the user to sort data by time and sensor type

#### Extra Tasks Chosen

- allows the user to filter data by sensor type.
- aggregate the data from all readings for the same sensor type, and compute the median of all its values. e.g

#### Explanation of approach

React was chosen due to my competency in the technology and the time constraints of the task. I felt I would be able to commit the highest quality code using a technology I was already very familiar with. I am of course keen to learn Angular in the future however.

Material UI component library was chosen so as to have a consistent design across the project, as well as to leverage the API of the Table component offering swifter implementation of features such as sorting, pagination, etc.

As I chose the feature-set which involved implementing two separate tables, I abstracted a separate reusable DataTable component.

The filtering was implemented to return all items which include the search term as a substring. Filtering is case insensitive.

For the additional task of the Median Table, the median was calculated by aggregating the values of each box/sensor-type in a sorted array. This was optimized by keeping the array sorted as items were inserted as opposed to sorting the array after aggregation.

#### Running the project

1. Clone repository to local environment
2. Ensure Yarn package manager is installed (https://classic.yarnpkg.com/en/docs/install)
3. run `yarn` to install dependencies
4. `yarn start` to run project
5. `yarn test` to run tests
