export type Order = "asc" | "desc";

export type Column = {
  id: string;
  label: string;
};

export type Reading = {
  id: string;
  name: string;
  sensor_type: string;
  box_id: string;
  unit: string;
  range_l: number;
  range_u: number;
  longitude: number;
  latitude: number;
  reading: number;
  reading_ts: Date;
};
