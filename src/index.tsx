import React from "react";
import { render } from "react-dom";
import SensorReadings from "./components/sensor-readings";

render(<SensorReadings />, document.getElementById("root"));
