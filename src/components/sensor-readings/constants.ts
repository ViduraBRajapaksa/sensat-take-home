import { Column, Reading } from "../../types";
import data from "../../data/sensor_readings.json";
import { getMedian, insertSorted } from "./utils";

const MAIN_COLUMNS: Column[] = [
  {
    id: "id",
    label: "ID",
  },
  {
    id: "name",
    label: "Name",
  },
  {
    id: "sensor_type",
    label: "Sensor Type",
  },
  {
    id: "box_id",
    label: "Box ID",
  },
  {
    id: "unit",
    label: "Unit",
  },
  {
    id: "range_l",
    label: "Range Lower Bound",
  },
  {
    id: "range_u",
    label: "Range Upper Bound",
  },
  {
    id: "latitude",
    label: "Latitude",
  },
  {
    id: "longitude",
    label: "Longitude",
  },
  {
    id: "reading",
    label: "Reading",
  },
  {
    id: "reading_ts",
    label: "Reading Timestamp",
  },
];
const MEDIAN_COLUMNS: Column[] = [
  {
    id: "id",
    label: "ID",
  },
  {
    id: "sensor_type",
    label: "Sensor Type",
  },
  {
    id: "median",
    label: "Median",
  },
  {
    id: "unit",
    label: "Unit",
  },
];

const MAIN_TABLE_SORTABLE_FIELDS = ["reading_ts", "sensor_type"];

const readings = JSON.parse(JSON.stringify(data)).map((reading: Reading) => ({
  ...reading,
  row_id: `${reading.id}-${reading.box_id}-${reading.reading_ts}`,
}));

const medians = Object.entries(
  readings.reduce((accu: any, reading: Reading) => {
    if (!reading.sensor_type || !reading.reading) return accu;

    let id = `${reading.box_id}-${reading.sensor_type}`;

    if (!accu[id]) {
      accu[id] = {
        readings: [reading.reading],
        unit: reading.unit,
        sensor_type: reading.sensor_type,
        median: reading.reading,
      };
    } else {
      let newReadings = insertSorted(reading.reading, accu[id].readings);
      accu[id].readings = newReadings;
      accu[id].median = getMedian(newReadings);
    }

    return accu;
  }, {})
).map(([key, value]: [string, any]) => {
  return {
    id: key,
    row_id: key,
    ...value,
  };
});

export {
  MAIN_COLUMNS,
  MEDIAN_COLUMNS,
  MAIN_TABLE_SORTABLE_FIELDS,
  readings,
  medians,
};
