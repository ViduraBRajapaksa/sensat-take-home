import React from "react";
import { render, fireEvent } from "@testing-library/react";
import "@testing-library/jest-dom/extend-expect";
import SensorReadings from "./sensor-readings";

describe("Sensor Readings", () => {
  let rendered: any;

  beforeEach(() => {
    rendered = render(<SensorReadings />);
  });

  it("should render app without crashing", () => {
    expect(rendered.getByText("Readings Table")).toBeInTheDocument();
    expect(rendered.getByText("Median Table")).toBeInTheDocument();
  });

  it("should render all neccesary table columns", () => {
    expect(rendered.getAllByText("ID")[0]).toBeInTheDocument();
    expect(rendered.getAllByText("Sensor Type")[0]).toBeInTheDocument();
    expect(rendered.getAllByText("Unit")[0]).toBeInTheDocument();
    expect(rendered.getByText("Name")).toBeInTheDocument();
    expect(rendered.getByText("Box ID")).toBeInTheDocument();
    expect(rendered.getByText("Range Lower Bound")).toBeInTheDocument();
    expect(rendered.getByText("Range Upper Bound")).toBeInTheDocument();
    expect(rendered.getByText("Latitude")).toBeInTheDocument();
    expect(rendered.getByText("Longitude")).toBeInTheDocument();
    expect(rendered.getByText("Reading")).toBeInTheDocument();
    expect(rendered.getByText("Reading Timestamp")).toBeInTheDocument();
  });

  it("should correctly filter data on input", () => {
    const filterInput = rendered.getByPlaceholderText("Filter by sensor_type");
    expect(rendered.getAllByText("Carbon monoxide")[0]).toBeInTheDocument();
    expect(rendered.queryByText("Ambient temperature")).not.toBeInTheDocument();

    fireEvent.change(filterInput, {
      target: { value: "temp" },
    });

    expect(rendered.getAllByText("Ambient temperature")[0]).toBeInTheDocument();
    expect(rendered.queryByText("Carbon monoxide")).not.toBeInTheDocument();
  });
});
