import React from "react";
import DataTable from "../data-table";
import { Grid, Typography, Box } from "@material-ui/core";
import {
  MAIN_COLUMNS,
  MEDIAN_COLUMNS,
  MAIN_TABLE_SORTABLE_FIELDS,
  readings,
  medians,
} from "./constants";

export default function SensorReadings() {
  return (
    <Grid>
      <Typography>Readings Table</Typography>
      <DataTable
        data={readings}
        filter="sensor_type"
        columns={MAIN_COLUMNS}
        sortableFields={MAIN_TABLE_SORTABLE_FIELDS}
      />
      <Box />
      <Typography>Median Table</Typography>
      <DataTable data={medians} columns={MEDIAN_COLUMNS} sortableFields={[]} />
    </Grid>
  );
}
