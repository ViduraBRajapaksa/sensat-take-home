const locationOf = (
  element: number,
  array: number[],
  start?: number,
  end?: number
): number => {
  start = start || 0;
  end = end || array.length;
  var pivot = Math.floor(start + (end - start) / 2);
  if (end - start <= 1 || array[pivot] === element) return pivot;
  if (array[pivot] < element) {
    return locationOf(element, array, pivot, end);
  } else {
    return locationOf(element, array, start, pivot);
  }
};

const getMedian = (arr: number[]) => {
  const mid = Math.floor(arr.length / 2);
  return arr.length % 2 !== 0 ? arr[mid] : (arr[mid - 1] + arr[mid]) / 2;
};

const insertSorted = (element: number, array: number[]) => {
  array.splice(locationOf(element, array), 0, element);
  return array;
};

export { insertSorted, getMedian };
